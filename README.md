A PDF version of the talk can be found in
[festplattencrypto.pdf](festplattencrypto.pdf).

# Deployment

To run the presentation just go to your local clone of the
[reveal.js](https://github.com/hakimel/reveal.js/) package and link
the talk and the resources. Afterwards, install all requirements of
the reveal.js package and start a npm server.

``` bash
## In the root of the reveal.js project
mv index.html old.html
ln -s PATH_TO_PRESENTATION_FOLDER/festplatten-crypto.html index.html
ln -s PATH_TO_PRESENTATION_FOLDER/res res

## Install all package requirements and run the npm server
npm install
npm start
```

# References

- [LUKS/Cryptsetup](https://gitlab.com/cryptsetup/cryptsetup/-/wikis/home)
- [shred](https://linux.die.net/man/1/shred)
- [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)
- [VeraCrypt - hidden volumes](https://www.veracrypt.fr/en/Hidden%20Volume.html)
- [VeraCrypt - hidden operation system](https://www.veracrypt.fr/en/VeraCrypt%20Hidden%20Operating%20System.html)

# License

All material published in this repository - except of the two pictures
taken from
https://www.veracrypt.fr/en/VeraCrypt%20Hidden%20Operating%20System.html
and https://www.veracrypt.fr/en/Hidden%20Volume.html - is licensed
under [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
(public domain).
